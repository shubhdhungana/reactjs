import React, { useState } from 'react'

const HideAndShowImg = () => {
    let [show, setShow] = useState(true)
  return (
    <div>
        {show? <img src="./favicon.ico"></img>:null}
        <br></br>
        <button
            onClick={()=>{
                setShow(true)
            }}
        >Show</button>
        <br></br>


        <button
        onClick={(e)=>{
            setShow(false)
        }}
        >Hide</button>
    </div>
  )
}

export default HideAndShowImg