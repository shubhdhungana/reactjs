import React, { useState } from 'react'

const ImageButton = () => {
    let[show, setShow] = useState(true)

  return (
    <div>
        {show ? <img src="./logo192.png"></img> : null}
        <button
        onClick={(e)=>{
            if(show){
                setShow(false)
            }else{
                setShow(true)
            }
        }}
        >
            Show Image
        </button>
    </div>
  )
}

export default ImageButton