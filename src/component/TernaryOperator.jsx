import React from 'react'

const TernaryOperator = ({isMarried, age, marks}) => {
    let calc = (age) =>{
        if(age<18){
            return <div>He is underage</div>
        }else if (age >= 18 && age<60) {
            return <div>He is Adult</div>
        }else {
            return <div>He is old</div>
        }
    }

    //if else marks checker
    let marksChecker = (marks) =>{
        if (marks<=39) {
            return <div>failed</div>
        }else if (marks>=40&&marks<=60) {
            return <div>second division</div>
        }else if (marks>=70&&marks<=80){
            return <div>first division</div>
        }else{
            return <div>distinction</div>
        }
    }

  return (


    <div>
        {marksChecker(marks)}
        
        {
            marks===39?(<div>failed</div>):
            marks>=40&&marks<=59?(<div>second division</div>):
            marks>=60&&marks<=79?(<div>first division</div>):
            marks>=80&&marks<=100?(<div>distinction</div>):
            null
        }


        {calc(age)}

        
        {
          isMarried === true ? (<div>Married</div>) : (<div>Unmarried</div>)  
        }


        {
            age<18 ? <div>He is underage</div>
            : age>18&&age<60 ?    <div>He is an adult</div>
            :   <div>He is old</div>
        }
        

        {
            age>18?<div>He can enter bar </div>
            : null
        }


    </div>
  )
}

export default TernaryOperator