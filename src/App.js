import Height from "./Height"
import Home from "./Home"
import Weight from "./Weight"
import { Age } from "./age"
import Button from "./component/Button"
import HideAndShowImg from "./component/HideAndShowImg"
import ImageButton from "./component/ImageButton"
import Info from "./component/Info"
import Location from "./component/Location"
import MapMethod from "./component/MapMethod"
import NavMenu from "./component/NavMenu"
import OnClick1 from "./component/OnClick1"
import OnClick2 from "./component/OnClick2"
import Rendering1 from "./component/Rendering1"
import Rendering2 from "./component/Rendering2"
import RenderingPractise1 from "./component/RenderingPractise1"
import Ternary2 from "./component/Ternary2"
import TernaryOperator from "./component/TernaryOperator"
import ToggleImg from "./component/ToggleImg"
import CleanUpFunc from "./component/useEffect/CleanUpFunc"
import LearnUseEffect1 from "./component/useEffect/LearnUseEffect1"

import { Name } from "./name"

let App = ()=>{
  return (
    <div>

      <p className="success">this is paragraph</p>
      {/*
        keywords in react
        -----------------------
        p => tag => element
        "this is paragraph" => children
        className = props
      */}

      {/* <h1 style={{backgroundColor:"orange",color:"white"}}> This is is heading tag</h1> */}
      
     {/*  <p className="red">This is paragraph tag</p> */}
     {/*  <p className="snapchat">This is another paragraph</p> */}
      {/* <p className="blue">This is third paragraph</p> */}
      
      {/* <h2 style={{backgroundColor:"black", color:"white"}}>This is h2</h2> */}
      
      {/* <a href="https://www.youtube.com/" target=" ">Youtube</a> */}
    {/*   <img src="./favicon.ico"></img> */}

     {/*  <Name></Name> */}
      {/* <Age></Age> */}
    {/*   <Weight></Weight> */}
      {/* <Height></Height> */}
     {/*  <Info name="subh" address="lalitpur" college="ismt" age={29} isMarried={true}></Info> */}
     {/*  <Info2></Info2> */}
     {/*  <Home value="rs. 2 Crore" address="lalitpur" floor={3}></Home> */}
    
{/*       <NavMenu name="subha" age={24} height="5'10"></NavMenu> */}
{/*       <Ternary2 isMarried={false} age={7} marks={95}></Ternary2> */}
      {/* <TernaryOperator isMarried = {true} age={19} marks={90}></TernaryOperator> */}

     {/*  <MapMethod></MapMethod>  */}
   {/*    <OnClick1></OnClick1> */}
  
{/*   <LearnUseState1></LearnUseState1> */}
 {/*  <HideAndShowImg></HideAndShowImg> */}
{/*  <ToggleImg></ToggleImg> */}
{/* <Button></Button> */}
{/* <ImageButton></ImageButton> */}

{/* <OnClick2></OnClick2> */}
{/* <Rendering1></Rendering1> */}
{/* <Rendering2></Rendering2> */}
{/* <RenderingPractise1></RenderingPractise1> */}
{/* <LearnUseEffect1></LearnUseEffect1> */}
<CleanUpFunc></CleanUpFunc>
    </div>
  )
}


export default App

