import React, { useState } from 'react'

const ToggleImg = () => {
    let [show, setShow] = useState(true)
  return (
    <div>
        {show ? <img src="./favicon.ico"></img>: null}

        <button
        onClick={(e)=>{
            if(show){
                setShow(false)
            }else {
                setShow(true)
            } 

        }}
        >Toggle</button>
    </div>
  )
}

export default ToggleImg