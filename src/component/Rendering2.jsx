import React, { useState } from 'react'

const Rendering2 = () => {
    let [count1, setCount1] = useState(0)
    let [count2, setCount2] = useState(100)


    const incrementCount1 = (e) => {
        setCount1(count1 + 1)
    }

    const incrementCount2 = (e)=>{
        setCount2(count2+100)
    }
  return (
    <div>
        
    count 1 is {count1} <br></br>
    count 2 is {count2} <br></br>
        <button
        onClick={incrementCount1}
        >count1</button> 

        <button
        onClick={incrementCount2}
        >count2</button>
    </div>
  )
}

export default Rendering2