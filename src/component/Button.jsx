import React, { useState } from 'react'

const Button = () => {
    let [count, setCount] = useState(1)
    return (
    <div>
        {count} <br></br>

        {/*If value is less than 0, then set it to 0*/}

        <button onClick={(e)=>{
            if(count<1){
                setCount(0)
            }else {
                setCount(count-1)
            }
        }}>
            Limit to 0
        </button> <br></br>

        {/*If value is 10, then stop increasing */}
        <button
        onClick={(e)=>{
            if (count<10){
                setCount(count+1)
            }else {
                setCount(10)
            }
        }}
        >
            Add But Upto 10
        </button> <br></br>



        {/* plus one button */}
        <button onClick={(e)=>{
            setCount(count+1)
        }}>
            plus one button
        </button>  <br></br> <br></br>


        {/* minus one button */}
        <button onClick={(e)=>{
            setCount(count-1)
        }}>
            Minus One
        </button> <br></br> <br></br>



    </div>
  )
}

export default Button