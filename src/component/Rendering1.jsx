import React, { useState } from 'react'

const Rendering1 = () => {
    let [count, setCount] = useState(0)

  return (
    <div>
        count is {count}
        <br></br>

        <button 
        onClick={(e)=>{
            setCount(count+1)
        }}
        >Increment</button>
    </div>
  )
}

export default Rendering1