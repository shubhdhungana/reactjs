import React, { useState } from 'react'

const LearnUseState1 = () => {
    let [count, setCount] = useState(0)
  return (
    <div>
        count is {count}
        <br></br>

    <button
        onClick={(e)=>{
            setCount(10);
        }}
    >Change count</button>

<br></br> <br></br>

    <button
    onClick={(e)=>{
        if(count <= 0){
            setCount(0)
        }else {
            setCount(count-1)
        }
    }} 
    >Decrement Count</button>

<br></br>
<br></br>

{/*
 <button onClick{()=>{seCount()}}></button>
 */}
    <button
    onClick={(e)=>{
        setCount(count+1)
    }}
    >Increment count</button>


    {/*Stop Increment if the count is greater than 10 */}
    <br></br>
    <button
    onClick={(e)=>{
        if (count >= 10){
            setCount(10)
        } else {
            setCount(count+1)
        }
    }}
    >Increment then stop</button>

    </div>
  )
}

export default LearnUseState1