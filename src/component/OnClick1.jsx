import React from 'react'

const OnClick1 = () => {
    let sum = ()=>{
        console.log(1+2)
    }
  return (
    <div>
        <button
        onClick={()=>{
            console.log(1+2)
        }}
        >
            Sum
        </button>

        <button
        onClick={sum}
        >
            onclick sum
        </button>
    </div>
  )
}

export default OnClick1