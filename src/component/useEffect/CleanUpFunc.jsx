import React, { useEffect, useState } from 'react'

const CleanUpFunc = () => {
    let [count1, setCount1] = useState(0)
    let [count2, setCount2] = useState(100)
  
    let handleIncrement = ()=>{
      setCount1(count1+1)
    }
  
    let handleIncrement2 = ()=>{
      setCount2(count2+100)
    }
  
    useEffect(()=>{
      console.log("i am useEffect ")

      return ()=>{
        console.log("i am clean up function")
      }
    }, [count1])
  
    
    console.log("i am component")
  
  return (
    <div>
              count1 is {count1} <br></br>
      count2 is {count2} <br></br>
      <button
      onClick={handleIncrement}
      >Increment Count 1</button>

      <button
      onClick={handleIncrement2}
      >
        Increment Count 2
      </button>

    </div>
  )
}

export default CleanUpFunc